﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.main
{
	public struct TouchInformation
	{
		public bool IsTouch;
		public TouchPhase Phase;
		public Vector2 FirstTouch;
		public Vector2 NowTouch;
		public Vector2 DifferenceTouchPos
		{
			get
			{
				return NowTouch - FirstTouch;
			}
		}
		public float TouchAngle
		{
			get
			{
				return Mathf.Atan2(DifferenceTouchPos.y, DifferenceTouchPos.x) * Mathf.Rad2Deg;
			}
		}
	}
	public class TouchManager : MonoBehaviour
	{
		const short MaxTouch = 2;
		public TouchInformation[] TouchInfo = new TouchInformation[MaxTouch];
		public TouchEffect[] touchEffect;
		public void Start()
		{

		}
		public void Update()
		{
			int index;

			index = 0;
			while (index < MaxTouch)
			{
				TouchInfo[index].IsTouch = false;
				index++;
			}

			index = 0;
			while (index < MaxTouch && index < Input.touchCount)
			{
				int id = Input.touches[index].fingerId;
				TouchInfo[id].IsTouch = true;
				TouchInfo[id].Phase = Input.touches[index].phase;
				TouchInfo[id].NowTouch = Input.touches[index].position;
				if (TouchInfo[id].Phase == TouchPhase.Began)
				{
					TouchInfo[id].FirstTouch = TouchInfo[id].NowTouch;
					touchEffect[id].gameObject.SetActive(true);
					touchEffect[id].rect.position = TouchInfo[id].NowTouch;
				}
				else if (TouchInfo[id].Phase == TouchPhase.Ended)
				{
					touchEffect[id].gameObject.SetActive(false);
				}
				index++;
			}
		}
	}
}
