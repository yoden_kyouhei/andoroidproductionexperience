﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.main
{
	public class TouchEffect:MonoBehaviour
	{
		public RectTransform rect;
		public void Start()
		{
			rect = GetComponent<RectTransform>();
		}
		public void Update()
		{
			if (rect.sizeDelta.x < 400)
			{
				rect.sizeDelta += new Vector2(2000, 2000) * Time.deltaTime;
			}
			else
			{
				rect.sizeDelta = new Vector2(400, 400);
			}
		}
		public void OnEnable()
		{
			rect.sizeDelta = new Vector2(0, 0);
		}
	}
}
